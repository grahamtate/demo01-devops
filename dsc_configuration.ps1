Configuration webserver {
    Param(
        [Parameter()]
        [string[]]$Computername = "localhost"
    )

    Import-DscResource -ModuleName 'PSDesiredStateConfiguration'

    $logPath = "C:\logs"
    $logFile = "dsc.log"
    $log = Join-Path $logPath $logFile

    Node $Computername{
        File LogPath{
            DestinationPath = $logPath
            Ensure = 'Present'
            Type = 'Directory'
        }

        WindowsFeature webserver{
            Name = 'web-server'
            Ensure = 'Present'
            LogPath = $log
            IncludeAllSubFeature = $true
        }
    }
}

webserver

