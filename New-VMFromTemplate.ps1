Function New-VMFromTemplate {
    Param(
        [Parameter(Mandatory=$true)]
        [string[]]$Name,
        [Parameter(Mandatory=$true)]
        [ValidateSet("Server2016","Server2012R2")]
        [string]$Template,
        [Parameter(Mandatory=$true)]
        [string]$Node
    )
    $vmRootPath = "V:\virtual_machines"
    switch ($template){
        "Server2016" {$baseVhdx = "S:\vm_templates\server2016.vhdx"}
        "Server2012R2" {$baseVhdx = "S:\vm_templates\server2012r2.vhdx"}
    }
    ForEach ($vm in $name){
        If (Get-VM -Name $vm -ComputerName $node -ErrorAction SilentlyContinue){
            Write-Verbose "$vm already exists on $node"
            Continue
        }

        $path = [io.path]::Combine($vmRootPath,$vm)
        If (-not (Test-Path -Path $path)){
            Write-Verbose -Message "Path not found. Creating..."
            New-Item -Path $vmRootPath -Name $vm -ItemType Directory -Force | Out-Null
        }
        
        $osDisk = [io.path]::Combine($vmRootPath,$vm,"os.vhdx")
        If (-not (Test-Path -Path $osDisk)){
            Write-Verbose -Message "$osDisk not found. Copying..."
            Copy-Item -Path $baseVhdx -Destination $osDisk | Out-Null
        }

        Write-Verbose "Customizing $vm ..."
        $unattendXml = [io.path]::Combine($vmRootPath,$vm,"unattend.xml")
        (Get-Content S:\iso\unattend.xml).replace(';Computername;', $vm) | Set-Content $unattendXml

        $mount = "V:\$vm"
        If (-not (Test-Path $mount)){
            New-Item -Path $mount -ItemType Directory -Force | Out-Null
        }
        Mount-WindowsImage -ImagePath $osDisk -Path $mount -Index 1 | Out-Null
        Copy-Item -Path $unattendXml -Destination "$($mount)\Windows\Panther" | Out-Null
        Dismount-WindowsImage -Path $mount -Save | Out-Null

        Write-Verbose "Creating $vm ..."
        New-VM -Name $vm -MemoryStartupBytes 4GB -SwitchName "CNSwitch" -VHDPath $osDisk -Path $path -Generation 2 | Start-VM | Out-Null
    }
}

