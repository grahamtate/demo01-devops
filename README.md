# README #

### What is this repository for? ###

Configuration as Code for 2 Windows Server 2016 webservers. This is designed as my 1st attempt in to following devops mentality and the release pipeline model.

Will use Powershell as the primary language
Appveyor for the build aspect.
Tests will be designed using Pester
Notifications will be delivered via email and hipchat

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact